import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as mdates
import matplotlib.ticker as mticker
from matplotlib import ticker
import re
import sys


"""
  Read a log file from the IOC, having lines like this:
  "SES-SCAN:MC-MCU-001:m1-RBV-UTC    2021-07-02 15:03:15.413  243.123 "
"""

debug = 0


def main(argv=None):
    global debug
    if not argv:
        argv = sys.argv
    argc = len(argv)
    argvidx = 1
    while argv[argvidx] == "--debug":
        debug = debug + 1
        argvidx += 1
    if debug > 0:
        print("len(argv)=%u " % argc)
        print("argv=%s" % argv)
    dontUseGlitchDetector = False
    output_file_name = None
    input_file_name = argv[argvidx]
    argvidx += 1
    if len(argv) > argvidx:
        output_file_name = argv[argvidx]
    argvidx += 1

    fid = open(input_file_name)

    lines = fid.readlines()

    # Collect date into an X- and Y- array
    x = []
    y = []
    raw_x = None
    raw_y = None
    datetime_x = None
    datetime_y = None
    #                                    name      datetime             raw
    # RE_MATCH_IOC_LOG_LINE = re.compile(r"(\S+)\s+([0-9-]+\s+[0-9:.]+)\s+(.+)")
    RE_MATCH_IOC_LOG_LINE = re.compile(
        r"^([A-Za-z0-9:_-]+)\s+([0-9-]+\s+[0-9:.]+)\s+(.+)"
    )
    RE_MATCH_RAW_DIGITS_FLOATING_POINT = re.compile(r"([0-9-]+\.[0-9e+-]+)\s*$")
    RE_MATCH_RBV_UTC = re.compile(r"(.*)(RBV-UTC)$")
    RE_MATCH_EncAct_UTC = re.compile(r"(.*)(EncAct-UTC)$")
    # RBV-UTC
    # EncAct-UTC
    # RE_MATCH_RAW_DIGITS_NO_DOT_AND_SIGN = re.compile(r"([0-9-]+)\s*$")

    for i in range(0, len(lines)):
        line = lines[i]
        try:
            match_ioc_log_line = RE_MATCH_IOC_LOG_LINE.match(line)
            if match_ioc_log_line == None:
                print("re_match == None line=%s" % (line))
                continue
            pvname = match_ioc_log_line.group(1)
            match_raw_RE_MATCH_RBV_UTC = RE_MATCH_RBV_UTC.match(pvname)
            match_raw_RE_MATCH_EncAct_UTC = RE_MATCH_EncAct_UTC.match(pvname)
            if debug > 2:
                print(
                    f"pvname={pvname} match_raw_RE_MATCH_RBV_UTC={match_raw_RE_MATCH_RBV_UTC} match_raw_RE_MATCH_EncAct_UTC={match_raw_RE_MATCH_EncAct_UTC}"
                )

            datetime = match_ioc_log_line.group(2)
            raw = match_ioc_log_line.group(3)
            if debug > 2:
                print(
                    f'match_ioc_log_line={match_ioc_log_line} pvname={pvname} datetime={datetime} raw=""{raw}"" '
                )
            match_raw_DIGITS_FLOATING_POINT = RE_MATCH_RAW_DIGITS_FLOATING_POINT.match(
                raw
            )
            # match_raw_DIGITS_NO_DOT_AND_SIGN = (
            #    RE_MATCH_RAW_DIGITS_NO_DOT_AND_SIGN.match(raw)
            # )
            if debug > 2:
                print(
                    f"match_raw_DIGITS_FLOATING_POINT={match_raw_DIGITS_FLOATING_POINT}"
                )
                # print(
                #    f"match_raw_DIGITS_NO_DOT_AND_SIGN={match_raw_DIGITS_NO_DOT_AND_SIGN}"
                # )
            if match_raw_RE_MATCH_RBV_UTC != None:
                raw_x = float(raw)
                pvname_x = pvname
                datetime_x = datetime
            if match_raw_RE_MATCH_EncAct_UTC != None:
                # raw_y = float(raw)
                raw_y = int(float(raw))
                pvname_y = pvname
                datetime_y = datetime
            if raw_x != None and raw_y != None and datetime_x == datetime_y:
                if debug > 0:
                    print(f"line={1+i} raw_x={raw_x:.2f} raw_y={raw_y}")
                x.append(raw_x)
                y.append(raw_y)
                raw_x = None
                raw_y = None
                datetime_x = None
                datetime_y = None
        except ValueError:
            print("lines[i]=%s" % (lines[i]))
            print("lines[i].split=%s" % (lines[i].split(" ")))
            # sys.exit(1)

    fid.close()

    xnp = np.array(x)
    ynp = np.array(y)

    plt.plot(xnp, ynp)

    plt.xscale("linear")
    plt.yscale("linear")
    # plt.gca().xaxis.set_major_formatter(mticker.FormatStrFormatter("%f:0"))
    # plt.gca().yaxis.set_major_formatter(mticker.FormatStrFormatter("%d"))

    # plt.gca().yaxis.set_major_formatter(ticker.PercentFormatter(xmax=5))

    # plt.gca().yaxis.set_major_formatter("${x:1.2f}")

    # plt.gca().yaxis.set_major_formatter(mticker.FormatStrFormatter("%f usec"))
    # yaxis.set_major_formatter('${x:1.2f}')
    # for txt in plt.gca().xaxis.get_majorticklabels():
    #    txt.set_rotation(270)

    # for txt in plt.gca().yaxis.get_majorticklabels():
    #    txt.set_rotation(270)

    # plt.tight_layout()
    plt.title(output_file_name)
    if output_file_name is not None:
        if debug > 0:
            print("output_file_name=%s" % (output_file_name))
        plt.savefig(output_file_name)
    plt.show()


if __name__ == "__main__":
    sys.exit(main(sys.argv))
